package checker

import (
	"gitlab.com/flex_comp/util"
)

type Check interface {
	Name() string
	Check(*util.ContextCE) bool
}

type CheckConf struct {
	Formula []interface{}   `json:"formula"`
	Args    [][]interface{} `json:"args"`
}
