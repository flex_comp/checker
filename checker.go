package checker

import (
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/util"
)

var (
	checker *Checker
)

func init() {
	checker = &Checker{
		lib: make(map[string]Check),
	}
	_ = comp.RegComp(checker)
}

type Checker struct {
	lib map[string]Check
}

func (c *Checker) Init(map[string]interface{}, ...interface{}) error {
	return nil
}

func (c *Checker) Start(...interface{}) error {
	return nil
}

func (c *Checker) UnInit() {

}

func (c *Checker) Name() string {
	return "checker"
}

func Reg(check Check) {
	checker.reg(check)
}

func (c *Checker) reg(check Check) {
	c.lib[check.Name()] = check
}

func UnReg(check Check) {
	checker.unReg(check)
}

func (c *Checker) unReg(check Check) {
	delete(c.lib, check.Name())
}

func (c *Checker) exec(name string, ctx *util.ContextCE) bool {
	e, ok := c.lib[name]
	if !ok {
		log.Warn("未找到检查器:", name)
		return false
	}

	return e.Check(ctx)
}

func Do(ctx *util.ContextCE, formula []interface{}, args [][]interface{}) bool {
	return checker.do(ctx, formula, 0, args)
}

func (c *Checker) do(ctx *util.ContextCE, formula []interface{}, offset int, args [][]interface{}) bool {
	if len(formula) == 0 {
		// 无检查 = 通过
		return true
	}

	if len(formula) == 2 || len(formula) > 3 {
		log.Warn("异常配置在:", args)
		return false
	}

	left := formula[0]
	var lResult bool
	switch left.(type) {
	case string:
		a := args[offset]
		offset++
		ctx.Args = a
		lResult = c.exec(left.(string), ctx)
	case []interface{}:
		lResult = c.do(ctx, left.([]interface{}), offset, args)
	}

	if len(formula) == 1 {
		// 单个检查
		return lResult
	}

	// and or 检查
	op := util.ToString(formula[1])
	switch op {
	case "and":
		if !lResult {
			return false
		}
	case "or":
		if lResult {
			return true
		}
	default:
		log.Warn("异常配置在:", args)
		return false
	}

	right := formula[2]
	var rResult bool
	switch right.(type) {
	case string:
		a := args[offset]
		offset++
		ctx.Args = a
		rResult = c.exec(right.(string), ctx)
	case []interface{}:
		rResult = c.do(ctx, right.([]interface{}), offset, args)
	}

	return rResult
}
