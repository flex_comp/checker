package checker

import (
	"fmt"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/util"
	"io/ioutil"
	"os"
	"testing"
	"time"
)

type c1 struct{}

func (c *c1) Name() string {
	return "c1"
}

func (c *c1) Check(ctx *util.ContextCE) bool {
	fmt.Println("c1 check begin.")
	s, _ := util.JSONMarshal(ctx)
	fmt.Println(string(s))
	fmt.Println("c1 check end.")
	return false
}

type c2 struct{}

func (c *c2) Name() string {
	return "c2"
}

func (c *c2) Check(ctx *util.ContextCE) bool {
	fmt.Println("c2 check begin.")
	s, _ := util.JSONMarshal(ctx)
	fmt.Println(string(s))
	fmt.Println("c2 check end.")
	return true
}

type c3 struct{}

func (c *c3) Name() string {
	return "c3"
}

func (c *c3) Check(ctx *util.ContextCE) bool {
	fmt.Println("c3 check begin.")
	s, _ := util.JSONMarshal(ctx)
	fmt.Println(string(s))
	fmt.Println("c3 check end.")
	return true
}

type c4 struct{}

func (c *c4) Name() string {
	return "c4"
}

func (c *c4) Check(ctx *util.ContextCE) bool {
	fmt.Println("c4 check begin.")
	s, _ := util.JSONMarshal(ctx)
	fmt.Println(string(s))
	fmt.Println("c4 check end.")
	return true
}

type CheckerConf struct {
	Checker *CheckConf `json:"checker"`
}

func TestDo(t *testing.T) {
	_ = comp.Init(map[string]interface{}{
		"config": "./test/server.json",
	})

	Reg(new(c1))
	Reg(new(c2))
	Reg(new(c3))
	Reg(new(c4))

	srcFile, e := os.OpenFile("./test/formula.json", os.O_RDWR, 0666)
	if e != nil {
		t.Failed()
	}

	defer srcFile.Close()
	raw, e := ioutil.ReadAll(srcFile)
	if e != nil {
		t.Failed()
	}

	conf := new(CheckerConf)
	e = util.JSONUnmarshal(raw, conf)
	if e != nil {
		t.Failed()
	}

	go func() {
		<-time.After(time.Second)
		type args struct {
			ctx     *util.ContextCE
			formula []interface{}
			args    [][]interface{}
		}
		tests := []struct {
			name string
			args args
			want bool
		}{
			{
				name: "all",
				args: args{
					ctx: &util.ContextCE{
						Custom:      map[string]interface{}{"foo": "bar"},
						LocalCustom: nil,
						Config:      nil,
						External:    nil,
						Args:        nil,
					},
					formula: conf.Checker.Formula,
					args:    conf.Checker.Args,
				},
				want: true,
			},
		}
		for _, tt := range tests {
			t.Run(tt.name, func(t *testing.T) {
				if got := Do(tt.args.ctx, tt.args.formula, tt.args.args); got != tt.want {
					t.Errorf("Do() = %v, want %v", got, tt.want)
				}
			})
		}
	}()

	ch := make(chan bool, 1)
	go func() {
		<-time.After(time.Second * 10)
		ch <- true
	}()

	_ = comp.Start(ch)
}
